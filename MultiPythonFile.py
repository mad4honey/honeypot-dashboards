import re
import json
import time
import os
from urllib2 import urlopen
WorkingDir = os.getcwd()
print("Working Directory is: ", WorkingDir)
LogCounter = 0
print("Program Starting...!")
#*************************************************************************** IP Address Coordiantes ************************************************************************************
while True:
    def FindIP(HostileIP):
        try:
            url = 'http://ip-api.com/json/' + HostileIP
            #print(url)
            response = urlopen(url)
            data = json.load(response)

            IP = data['ip']
            lat = data['latitude']
            lon = data['longitude']

            #print('Your IP detail\n ')
           # print('IP : {4} \nRegion Name : {1} \nCountry : {2} \nCity : {3} \nOrg : {0}'.format(org,RegionName,country,city,IP))
            #print(lat,lon)
            #NewCoord = ("[" + lat + "," + lon + "]")
            #print NewCoord
            NewCoord = (lat, ",", lon)
            NewCoord1 = " ".join(map(str, NewCoord))
            formatString = ('[' + NewCoord1 + ']' + ',')
            with open(WorkingDir + '/assets/JSON_files/mapdata.json', 'r') as file:
                data = file.readlines()
            data[2] = formatString + '\n'

            # and write everything
            with open(WorkingDir + '/assets/JSON_files/mapdata.json', 'w') as file:
                data[2] = '\n' + formatString + '\n'
                file.writelines(data)

        except:
            #print("NO IP Match Found or No Internet Connection")
            IPTryingAgain(HostileIP)

    def IPTryingAgain(HostileIP):
        #print("Running IP ADDRESS FINDER!")
        try:
            url = ('http://api.ipstack.com/' + HostileIP + '?access_key=c5401669d53e824049fa97f8467948b9')
            #print(url)
            response = urlopen(url)
            data = json.load(response)
            #print(data)
            IP = data['ip']
            lat = data['latitude']
            lon = data['longitude']
            #print(IP, lat, lon)
            # print('Your IP detail\n ')
            #print('IP : {4} \nRegion Name : {1} \nCountry : {2} \nLatitude : {3} \nLongitude : {0}'.format(IP, region, country, lat, lon))
            #print(lat, lon)
            NewCoord = (lat, ",", lon)
            NewCoord1 = " ".join(map(str,NewCoord))
            formatString = ('[' + NewCoord1 + ']' + ',')
            with open(WorkingDir + '/assets/JSON_files/mapdata.json', 'r') as file:
                data = file.readlines()
            data[2] = formatString + '\n'

            # and write everything
            with open(WorkingDir + '/assets/JSON_files/mapdata.json', 'w') as file:
                data[2] = '\n' + formatString + '\n'
                file.writelines(data)
                #print ("FINSIHED IP FINDING!")

        except e:
            print(e)
            print("NO IP Match Found or No Internet Connection")

    #*************************************************************************** SNORT RULE CREATION ************************************************************************************
    def CreateSnortRules(ip):
        #print("****************************** Creating Snort Rule against " + ip + " ***********************************************")
        snort_ruleTCP = "alert tcp any any -> " + ip + " (flags: S; msg: 'Infected Host';)"
        snort_ruleUDP = "alert udp any any -> " + ip + " (flags: S; msg: 'Infected Host';)"
        NewRule = True
        try:
            with open(WorkingDir + "/assets/Snort_Rules/Snort_Rules.txt", "r") as Snort:
                array = []
                for line in Snort:
                    array.append(line)
                for x in array:
                    if x.rstrip() == snort_ruleTCP:
                        NewRule = False
        except:
            print("No File Excists")
        if NewRule == True:
            Snort = open(WorkingDir + '/assets/Snort_Rules/Snort_Rules.txt', 'a+')
            #print(snort_ruleTCP)
            #print(snort_ruleUDP)
            print>> Snort, snort_ruleTCP
            print>> Snort, snort_ruleUDP


    # ******************************************************************* MAIN SHIT **********************************************************
    def Start():
        data_file = open((WorkingDir + '/assets/JSON_files/cowrie.json'), 'a+') # POINT THIS FILE TO COWRIE.JSON!
        while True:
            try:
                line = data_file.readline()
                #print line
                ip = re.findall(r'[0-9]+(?:\.[0-9]+){3}', line)
                HostileIP = ip[0]
                print ip[0]
                IPTryingAgain(HostileIP)
                CreateSnortRules(HostileIP)
                if not line:
                    print("Dubug BOOm")
                    time.sleep(5)
            except:
                print("Finished! Sleep, Rave, Repeat")
                time.sleep(2)
    Start()
print("Program Unexpectedly Finished")